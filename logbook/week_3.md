# Engineering Logbook Week 03

| Item | Detail |
| ------ | ------ |
| Name | Mariam Nadira Binti Muhammad Johan |
| Matrics No | 196792 |
| Subsystem | Software & Control Systems |
| Group | 6 |
| Date | 1/11/2021 - 5/11/2021 |

## Weekly agenda and goals
Read research papers given by Azizi.


## Problem discussion
Still require further information and direction on what needs to be done.


## Decision making process
Try to arrange a meeting with Azizi.


## Decision justification
Require guidance from Azizi.


## Reflection
The research papers are hard to understand in the context of application for our project.


## What is the next step?
Hopefully meeting with Azizi.

# 
<div align="right">
[Next Week](https://gitlab.com/mariamjohan5/introduction/-/blob/main/logbook/week_4.md)
</div>
[Previous Week](https://gitlab.com/mariamjohan5/introduction/-/blob/main/logbook/week_2.md)
