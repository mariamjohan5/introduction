# Engineering Logbook Week 05

| Item | Detail |
| ------ | ------ |
| Name | Mariam Nadira Binti Muhammad Johan |
| Matrics No | 196792 |
| Subsystem | Software & Control Systems |
| Group | 6 |
| Date |  |

## Weekly agenda and goals
Went to the lab to meet with my groupmate, Aliff, so he could explain face-to-face regarding Mission Planner and the firmware PX4 as taught by Azizi.


## Problem discussion
We don't actually have anything to calibrate right now.


## Decision making process
Just familiarise myself with Mission Planner calibration and read up on PX4.


## Decision justification
We would not really be able to calibrate anything until the airship is ready.


## Reflection
Perhaps I could look up videos on Youtube on calibration using Mission Planner to get an idea it.


## What is the next step?
Learn more about the firmware, PX4

# 
<div align="right">
[Next Week](https://gitlab.com/mariamjohan5/introduction/-/blob/main/logbook/week_6.md)
</div>
[Previous Week](https://gitlab.com/mariamjohan5/introduction/-/blob/main/logbook/week_4.md)
