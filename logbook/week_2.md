# Engineering Logbook Week 02

| Item | Detail |
| ------ | ------ |
| Name | Mariam Nadira Binti Muhammad Johan |
| Matrics No | 196792 |
| Subsystem | Software & Control Systems |
| Group | 6 |
| Date | 25/10/2021 - 29/10/2021 |

## Weekly agenda and goals

Discussed project milestones and projected timeline spanning 14 weeks. Proceeded to create a [Gantt chart](https://docs.google.com/spreadsheets/d/1j9F4gbNZMCxQDzfhFpA2i7lVFxfhL9kVc-zxi1asfZs/edit#gid=1115838130) where all subsystems collaborated to share their projected timelines as certain subsystems require cooperation from other subsystems in order to proceed with their tasks.

## Problem discussion

<div align="middle">
<img src="assets/snc_targets.png" alt="targets" width="500"/>
</div>

Based on the main milestones given, as seen in the image above, we then needed to expand on the milestones as it was too general.

## Decision making process

Proceeded to create more specific and weekly based targets that were logically achievable to ensure continuous progress.

## Decision justification

So we would be able to complete the tasks step-by-step with enough time to resolve any issues before they snowball into a bigger problem.

## Reflection

Learned how to create a gantt chart and decide on logical milestones which would ensure a smooth project progression.

## What is the next step?

Getting started on the project based on the projected timeline as seen in the [Gantt chart](https://docs.google.com/spreadsheets/d/1j9F4gbNZMCxQDzfhFpA2i7lVFxfhL9kVc-zxi1asfZs/edit#gid=1115838130).

# 
<div align="right">
[Next Week](https://gitlab.com/mariamjohan5/introduction/-/blob/main/logbook/week_3.md)
</div>
