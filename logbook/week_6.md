# Engineering Logbook Week 06

| Item | Detail |
| ------ | ------ |
| Name | Mariam Nadira Binti Muhammad Johan |
| Matrics No | 196792 |
| Subsystem | Software & Control Systems |
| Group | 6 |
| Date |  |

## Weekly agenda and goals
Received update from Fikri on the new firmware we will be using which is [ArduHAU](https://docs.google.com/spreadsheets/d/1j9F4gbNZMCxQDzfhFpA2i7lVFxfhL9kVc-zxi1asfZs/edit#gid=1115838130).


## Problem discussion
Need to learn up on the ArduHAU firmware instead of PX4 now.


## Decision making process
Learn up on the ArduHAU firmware.


## Decision justification
It is the firmware we will be using.


## Reflection
Always need to be ready to adapt to changes in a project.


## What is the next step?


# 
<div align="right">
[Next Week]()
</div>
[Previous Week](https://gitlab.com/mariamjohan5/introduction/-/blob/main/logbook/week_5.md)
