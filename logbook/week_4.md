# Engineering Logbook Week 04

| Item | Detail |
| ------ | ------ |
| Name | Mariam Nadira Binti Muhammad Johan |
| Matrics No | 196792 |
| Subsystem | Software & Control Systems |
| Group | 6 |
| Date |  |

## Weekly agenda and goals
Meeting with Azizi at Lab H2.1 for further guidance. I was unable to attend as I had class at the time. However, my groupmates created a google drive to share what they learned and Aliff had also explained regarding Mission Planner.


## Problem discussion
It was my first time using Mission Planner.

## Decision making process
Familiarised myself with Mission Planner.


## Decision justification
Need to prepare myself for when we will be calibrating the airship to fly.


## Reflection
Downloaded and tried playing around with Mission Planner.


## What is the next step?
Meet face-to-face with groupmates to get a better explanation.

# 
<div align="right">
[Next Week](https://gitlab.com/mariamjohan5/introduction/-/blob/main/logbook/week_5.md)
</div>
[Previous Week](https://gitlab.com/mariamjohan5/introduction/-/blob/main/logbook/week_3.md)
