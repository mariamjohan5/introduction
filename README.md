<div align="middle">

## [**Weekly Logbooks**](logbook)

</div>

# Self introduction. yay 🥰
Hi, i'm Mariam Johan. Am experienced in being a living breathing human being for 22 years and counting. Not so good at intros sorry.

## A little bit about me 🙋‍♀️
<img src="assets/IMG_20200302_005442_483.jpg" alt="yeahme" width="200"/>

Name: Mariam Nadira Binti Muhammad Johan 

Where I live: Petaling Jaya, Selangor

## Some fun facts to spice things up
- Always down for outdoor activities
- Have a cute cat named Bluey
- Love pastel pink

## Strengths & Weaknesses 💪🏻

| No. | Strength | Weaknesses |
| ------ | ------ | ------ | 
| 1 | Work well under time constraint | Big procrastinator | 
| 2 | Good at adapting to situations | Kinda bad at organising | 
| 3 | Somewhat good at programming | Bad at stuff like structures |
